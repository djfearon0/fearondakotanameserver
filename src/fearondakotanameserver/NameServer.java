/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fearondakotanameserver;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Dakota F
 * NameServer accepts clients and creates new workers to handle client input
 */
public class NameServer implements Runnable {

    ServerSocket sock;//Create new socket for accepting clients
    int port;//Create new number for the port to be used
    boolean running;//Create new boolean to determine if running

    public NameServer() {
        port = 4000;//Initialize setting the port to 4000
        running = true;//Server is running
    }

    @Override
    public void run() {
        try {
            sock = new ServerSocket(port);//Initialize server socket with the initialized port
            ExecutorService myThreadPool;//Create thread pool for storing unused threads
            myThreadPool = Executors.newCachedThreadPool();//Initialize thread pool

            System.out.println("Server has started");//Indicate that the server has started and is looping
            
            while(running == true) {//Loop server

                Socket clientSock = sock.accept();//Accept client socket

                TranslationWorker work = new TranslationWorker(clientSock);//Create a new worker passing client socket for input
                myThreadPool.execute(work);//Put worker in new thread to execute
            }
            
            myThreadPool.shutdown();//Shut down the thread pool and release threads
            
        } catch (IOException e) {
            System.out.println("Server socket is closed");//Print when socket can not connect to server successfully
        }
    }
    
    public void stop() throws IOException{
        running = false;
        try{
        sock.close();//Close the server socket
        }
        catch(SocketException e){
            System.out.println("The server socket is closed.");//Print that server socket no longer accepts connections
        }
    }

}
