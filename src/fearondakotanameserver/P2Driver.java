/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fearondakotanameserver;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Dakota F
 */
public class P2Driver {

    NameServer server = new NameServer();//Initialize new server object
    Scanner scanner = new Scanner(System.in);//Initialize scanner for input

    public void start() throws InterruptedException, IOException {
        Thread serverThread = new Thread(server);//Create new thread for server object
        serverThread.start();//Execute server thread
        while (scanner.hasNextLine()) {//Check for any user input in display
            server.stop();//Signal server thread to stop looping
            serverThread.join();//Join thread
            System.out.println("Server has shut down");//Indicate server has stopped looping
            System.exit(0);//Exit with code 0
        }
    }

}
