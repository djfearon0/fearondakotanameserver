/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fearondakotanameserver;

import java.io.*;
import java.net.*;
import java.util.Scanner;

/**
 *
 * @author Dakota F
 */
public class TranslationWorker implements Runnable {

    Socket clientSocket;//Create socket for client
    PrintWriter pout;//Create PrintWriter for output to client

    public TranslationWorker(Socket client) throws IOException {
        clientSocket = client;//Initialize client socket based on parameter passed from NameServer class
        pout = new PrintWriter(clientSocket.getOutputStream(), true);//Initialize output to client
    }

    @Override
    public void run() {
        try {
            InputStream in = clientSocket.getInputStream();//Get client socket input stream
            Scanner scanner = new Scanner(in);//Initialize scanner for reading input stream

            InetAddress addr = InetAddress.getByName(scanner.nextLine());//Get address from client socket
            String IPAsString = addr.getHostAddress();//Convert to IP Address
            pout.println(IPAsString);//Print IP Address to client
            
            clientSocket.close();//Close the client socket
        } catch (IOException e) {
            try {
                pout.println("Could not resolve host name");//Output to client that IP Address could not be found
                clientSocket.close();//Close the client socket
            } catch (IOException ex) {
               System.out.println("Could not close client socket");//Print to display if client socket could not be closed
            }
                
        }
    }

}
